In  order to implement the following project 21 hours of work were required in total counting from inception to delivery.  
The timing includes all the setting up of the project, the graphical part, the implementation, the debugging and of course the mindmap which is included inside and solves it conceptually.

Typically this project was meant to be implemented using dotNET technologies which would be much faster for this b2b scenario but I am not currently familiar with those so the stack is this:

* Typescript
* HTML5
* CSS3
* Backbone
* Marionette

# How to Execute Instructions

You will need the node package module (npm) called **json-server** from here: [https://github.com/typicode/json-server](https://github.com/typicode/json-server)

After installing it locally execute in terminal: `json-server db.json`  
*make sure your current working directory in terminal is the same as the root folder of this project*

Currently this project runs only in latest Chrome (currently version 43). Update your browser from here if necessary: [http://outdatedbrowser.com/en](http://outdatedbrowser.com/en)

Open `table.html` in Chrome and enjoy!