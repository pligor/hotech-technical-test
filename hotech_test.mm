<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1355165260248" ID="ID_1078887940" MODIFIED="1433847600734" TEXT="hotech_test">
<node CREATED="1433841879628" ID="ID_269195059" MODIFIED="1433841886146" POSITION="left" TEXT="current version works only in CHROME"/>
<node CREATED="1433781853295" ID="ID_1359148195" MODIFIED="1433781891780" POSITION="left" TEXT="&#x395;&#x39d;&#x39d;&#x39f;&#x395;&#x399;&#x3a4;&#x391;&#x399; &#x3cc;&#x3c4;&#x3b9; &#x3c3;&#x3b5; &#x3c0;&#x3c1;&#x3b1;&#x3b3;&#x3bc;&#x3b1;&#x3c4;&#x3b9;&#x3ba;&#x3bf; project &#x3b4;&#x3b5;&#x3bd; &#x3b8;&#x3b1; &#x3b3;&#x3af;&#x3bd;&#x3bf;&#x3bd;&#x3c4;&#x3b1;&#x3b9; &#x3c5;&#x3c0;&#x3bf;&#x3b8;&#x3ad;&#x3c3;&#x3b5;&#x3b9;&#x3c2;. &#x3a4;&#x3ce;&#x3c1;&#x3b1; &#x3b3;&#x3af;&#x3bd;&#x3bf;&#x3bd;&#x3c4;&#x3b1;&#x3b9; &#x3c3;&#x3c4;&#x3b1; &#x3c0;&#x3bb;&#x3b1;&#x3af;&#x3c3;&#x3b9;&#x3b1; &#x3c4;&#x3b7;&#x3c2; &#x3ac;&#x3c3;&#x3ba;&#x3b7;&#x3c3;&#x3b7;&#x3c2;"/>
<node CREATED="1433781729384" ID="ID_1931486927" MODIFIED="1433782147717" POSITION="left" TEXT="&#x394;&#x395;&#x39d; &#x3b8;&#x3b1; &#x3b3;&#x3af;&#x3bd;&#x3b5;&#x3b9; &#x3c7;&#x3c1;&#x3ae;&#x3c3;&#x3b7; &#x3c3;&#x3c4;&#x3bf;&#x3b9;&#x3c7;&#x3b5;&#x3af;&#x3c9;&#x3bd; &#x3c0;&#x3bf;&#x3c5; &#x3b4;&#x3b5;&#x3bd; &#x3b3;&#x3bd;&#x3c9;&#x3c1;&#x3b9;&#x3b6;&#x3c9; &#x3b1;&#x3c5;&#x3c4;&#x3b7; &#x3c4;&#x3b7; &#x3c3;&#x3c4;&#x3b9;&#x3b3;&#x3bc;&#x3b7;">
<node CREATED="1433781756967" ID="ID_1852807939" MODIFIED="1433782132055" TEXT="TELERIK elements"/>
<node CREATED="1433781766615" ID="ID_1882344860" MODIFIED="1433781769621" TEXT="ASP.NET"/>
</node>
<node CREATED="1433781550237" ID="ID_1647921284" MODIFIED="1433781857384" POSITION="right" TEXT="&#x3c5;&#x3bb;&#x3bf;&#x3c0;&#x3bf;&#x3b9;&#x3b7;&#x3c3;&#x3b7; GRID">
<node CREATED="1433781787110" ID="ID_1684155068" MODIFIED="1433781800054" TEXT="(&#x394;&#x395;&#x39d; &#x3c5;&#x3c0;&#x3b1;&#x3c1;&#x3c7;&#x3b5;&#x3b9; screenshot)"/>
<node CREATED="1433781778943" ID="ID_664816490" MODIFIED="1433781843356" TEXT="&#x3ad;&#x3bd;&#x3b1; default html table element">
<node CREATED="1433781808247" ID="ID_1077409892" MODIFIED="1433781950140" TEXT="event id"/>
<node CREATED="1433781845175" ID="ID_737677841" MODIFIED="1433781920478" TEXT="edit button">
<node CREATED="1433783341588" ID="ID_1144739186" MODIFIED="1433783348410" TEXT="render the id inside somehow"/>
<node CREATED="1433783354372" ID="ID_1109831451" MODIFIED="1433783357474" TEXT="on click">
<node CREATED="1433783252108" ID="ID_1393437965" MODIFIED="1433783299995" TEXT="grab id from html"/>
<node CREATED="1433783397397" ID="ID_1903932552" MODIFIED="1433783403954" TEXT="set the id in session storage"/>
<node CREATED="1433783382069" ID="ID_1727989759" MODIFIED="1433783392627" TEXT="redirect to form page"/>
</node>
</node>
<node CREATED="1433781940103" ID="ID_43392399" MODIFIED="1433781946302" TEXT="delete button">
<node CREATED="1433783341588" ID="ID_36631289" MODIFIED="1433783348410" TEXT="render the id inside somehow"/>
<node CREATED="1433783378372" ID="ID_1027660013" MODIFIED="1433783379970" TEXT="on click">
<node CREATED="1433783413637" ID="ID_364256110" MODIFIED="1433783418639" TEXT="grab id from html"/>
<node CREATED="1433783419373" ID="ID_1092737498" MODIFIED="1433783486082" TEXT="delete from database"/>
<node CREATED="1433783424629" ID="ID_291354231" MODIFIED="1433783495559" TEXT="erase row"/>
</node>
</node>
</node>
<node CREATED="1433781925160" ID="ID_461777394" MODIFIED="1433781988941" TEXT="create button">
<node CREATED="1433781990136" ID="ID_568725388" MODIFIED="1433781996326" TEXT="below table"/>
<node CREATED="1433783516156" ID="ID_20040832" MODIFIED="1433783517875" TEXT="on click">
<node CREATED="1433783518693" ID="ID_1929065999" MODIFIED="1433783550969" TEXT="clear the id in session storage (just to make sure)"/>
<node CREATED="1433783555837" ID="ID_770990369" MODIFIED="1433783572618" TEXT="redirect to form page"/>
</node>
</node>
</node>
<node CREATED="1433782063614" ID="ID_1690239070" MODIFIED="1433782241311" POSITION="right" TEXT="how to pass id from one page to the other">
<node CREATED="1433782161041" ID="ID_1019568997" MODIFIED="1433782209782" TEXT="&#x3b5;&#x3c0;&#x3b5;&#x3b9;&#x3b4;&#x3b7; &#x3b4;&#x3b5;&#x3bd; &#x3c5;&#x3c0;&#x3b1;&#x3c1;&#x3c7;&#x3b5;&#x3b9; &#x3c0;&#x3c1;&#x3b1;&#x3b3;&#x3bc;&#x3b1;&#x3c4;&#x3b9;&#x3ba;&#x3bf;&#x3c2; server &#x3c4;&#x3bf; sessionStorage &#x3c4;&#x3bf;&#x3c5; browser &#x3b8;&#x3b1; &#x3b2;&#x3bf;&#x3b7;&#x3b8;&#x3b7;&#x3c3;&#x3b5;&#x3b9;"/>
</node>
<node CREATED="1433782082712" ID="ID_676552863" MODIFIED="1433782091954" POSITION="left" TEXT="&#x3b4;&#x3b5;&#x3bd; &#x3c5;&#x3c0;&#x3b1;&#x3c1;&#x3c7;&#x3b5;&#x3b9; &#x3c0;&#x3c1;&#x3b1;&#x3b3;&#x3bc;&#x3b1;&#x3c4;&#x3b9;&#x3ba;&#x3bf;&#x3c2; server">
<node CREATED="1433782091955" ID="ID_631515297" MODIFIED="1433782106838" TEXT="&#x3c5;&#x3c0;&#x3b1;&#x3c1;&#x3c7;&#x3b5;&#x3b9; JSON server &#x3c0;&#x3bf;&#x3c5; &#x3c4;&#x3c1;&#x3b5;&#x3c7;&#x3b5;&#x3b9; &#x3bc;&#x3bf;&#x3bd;&#x3bf; on memory"/>
</node>
<node CREATED="1433782372553" ID="ID_1014278471" MODIFIED="1433782559496" POSITION="right" TEXT="main layout will contain header and footer">
<node CREATED="1433782563041" ID="ID_801818972" MODIFIED="1433874744873" TEXT="Form Content is layout">
<node CREATED="1433782583881" ID="ID_758455275" MODIFIED="1433782646143" TEXT="main event setup is item view"/>
<node CREATED="1433782637137" ID="ID_451746312" MODIFIED="1433782644045" TEXT="event locations is collection view"/>
<node CREATED="1433782647266" ID="ID_1534878838" MODIFIED="1433782716616" TEXT="equipment requests is collection view"/>
</node>
</node>
<node CREATED="1433782405057" ID="ID_1548724578" MODIFIED="1433782453519" POSITION="left" TEXT="&#x394;&#x3b5;&#x3bd; &#x3c5;&#x3c0;&#x3ac;&#x3c1;&#x3c7;&#x3b5;&#x3b9; responsiveness &#x3c3;&#x3c4;&#x3b7; &#x3c3;&#x3b5;&#x3bb;&#x3af;&#x3b4;&#x3b1;">
<node CREATED="1433782454321" ID="ID_32814590" MODIFIED="1433782474541" TEXT="&#x3c4;&#x3b1; &#x3bc;&#x3b5;&#x3b3;&#x3ad;&#x3b8;&#x3b7; &#x3c4;&#x3c9;&#x3bd; elements &#x3b8;&#x3b1; &#x3c6;&#x3c4;&#x3b9;&#x3b1;&#x3c7;&#x3c4;&#x3bf;&#x3cd;&#x3bd; &#x3bc;&#x3b5; pixels">
<node CREATED="1433782475146" ID="ID_66749157" MODIFIED="1433782482408" TEXT="not recommended on a real project"/>
</node>
</node>
<node CREATED="1433782673978" ID="ID_1703750498" MODIFIED="1433782967434" POSITION="left" TEXT="no error handling for this form (out of the scope of this test, not in specs)"/>
<node CREATED="1433782743554" ID="ID_140326817" MODIFIED="1434018406386" POSITION="right" TEXT="dummy json server">
<node CREATED="1433782753971" ID="ID_1834674783" MODIFIED="1433782761217" TEXT="every event has an event id"/>
<node CREATED="1433782768074" ID="ID_1455194316" MODIFIED="1433782784529" TEXT="1 event has many event locations (1 to many)">
<node CREATED="1433782808307" ID="ID_921620362" MODIFIED="1433922953137" TEXT="each event location has eventId"/>
</node>
<node CREATED="1433782785587" ID="ID_1846288097" MODIFIED="1433782796374" TEXT="1 event has many equipment requests (1 to many)">
<node CREATED="1433782833099" ID="ID_979950374" MODIFIED="1433922957185" TEXT="each equipment request has eventId"/>
</node>
</node>
<node CREATED="1433782917291" ID="ID_1674094170" MODIFIED="1433782978270" POSITION="right" TEXT="onSave">
<node CREATED="1433782979035" ID="ID_1350963715" MODIFIED="1433783046618" TEXT="if old update"/>
<node CREATED="1433783040060" ID="ID_1670870579" MODIFIED="1433783049483" TEXT="if new create"/>
<node CREATED="1433783093986" ID="ID_418750914" MODIFIED="1433783097898" TEXT="children collections">
<node CREATED="1433783098411" ID="ID_1069359826" MODIFIED="1433783120963" TEXT="sync entire collection"/>
</node>
<node CREATED="1433783129548" ID="ID_44998034" MODIFIED="1433783135633" TEXT="on Success">
<node CREATED="1433783136180" ID="ID_1965501892" MODIFIED="1433783156010" TEXT="visit all events page"/>
<node CREATED="1433783170819" ID="ID_126840832" MODIFIED="1433783177831" TEXT="clear sessionStorage key"/>
</node>
</node>
<node CREATED="1433782925818" ID="ID_469234293" MODIFIED="1433782952706" POSITION="left" TEXT="on LOGOFF nothing happens (out of the scope of this test)"/>
<node CREATED="1433783159788" ID="ID_913410759" MODIFIED="1433783230753" POSITION="right" TEXT="if session storage key is available">
<node CREATED="1433783231444" ID="ID_71348095" MODIFIED="1433783239506" TEXT="fetch from server event values"/>
<node CREATED="1433783240333" ID="ID_1984096560" MODIFIED="1433783248449" TEXT="fetch all children collection"/>
</node>
<node CREATED="1433841018269" ID="ID_279123860" MODIFIED="1434018338719" POSITION="right" TEXT="how to pass event id to children models">
<node CREATED="1433923046394" ID="ID_381943009" MODIFIED="1434018347834" TEXT="new model">
<node CREATED="1434018348340" ID="ID_1415199892" MODIFIED="1434018351442" TEXT="new children">
<node CREATED="1434018358589" ID="ID_556192720" MODIFIED="1434018368987" TEXT="get it when saving on server"/>
</node>
</node>
<node CREATED="1434018353125" ID="ID_919798778" MODIFIED="1434018356123" TEXT="old model">
<node CREATED="1434018356692" ID="ID_1501397066" MODIFIED="1434018373827" TEXT="old children">
<node CREATED="1434018374492" ID="ID_1395248754" MODIFIED="1434018379779" TEXT="already have parent id"/>
</node>
<node CREATED="1434018381396" ID="ID_82315353" MODIFIED="1434018384339" TEXT="new children">
<node CREATED="1434018385005" ID="ID_423610701" MODIFIED="1434018389322" TEXT="get it when saving on server"/>
</node>
</node>
</node>
<node CREATED="1433841838539" ID="ID_920985864" MODIFIED="1433841864514" POSITION="left" TEXT="no widget is being used for dates. We use native html5 input for dates"/>
<node CREATED="1433951394115" ID="ID_1024104467" MODIFIED="1433951404721" POSITION="left" TEXT="no error handling for server failures"/>
<node CREATED="1433847601378" ID="ID_831636438" MODIFIED="1433847610303" POSITION="left" TEXT="gulp build system is NOT being used">
<node CREATED="1434014483764" ID="ID_1148768642" MODIFIED="1434014502397" TEXT="no JS concat and minification"/>
<node CREATED="1434014491743" ID="ID_481825670" MODIFIED="1434014506902" TEXT="no CSS concat and minification"/>
</node>
</node>
</map>
