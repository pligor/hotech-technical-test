/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/jquery.d.ts" />
/// <reference path="../../../declarations/underscore.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />
/// <reference path="../traits/ViewTraits.ts" />
/// <reference path="../typescript.ts" />
/// <reference path="../models/EquipmentRequestModel.ts" />

module Hotech {
    export class EquipmentRequestItemView extends Marionette.ItemView<EquipmentRequestModel> implements ViewTraits.BindingUiElements<EquipmentRequestModel> {

        template = "script#equipmentRequest_template" //http://marionettejs.com/docs/v2.4.1/marionette.itemview.html

        //if you have it as attributes they are not working
        /*this.tagName = "tr"
         this.className = "noname"
         this.id = "malakas"*/
        //you need to set them as functions instead
        tagName() {
            return "div"
        }

        className() {
            return "equipment_request"
        }

        ui() {
            //return super.ui();
            return {
                //currentYear: ".current_year"
            }
        }

        getUI:(arg:string) => JQuery;

        constructor(options?:Backbone.ViewOptions<EquipmentRequestModel>) {
            super(options)
        }

        onInitialize(options:Backbone.ViewOptions<EquipmentRequestModel>) {
            this.model.on("refreshYourself", () => {
                var prefix = "#eqRequest_"

                var jsonObj = this.model.toJSON()
                delete jsonObj.id
                delete jsonObj.eventId

                var keys = Object.getOwnPropertyNames(jsonObj)

                var newAttrs = {}

                keys.forEach((key) => {
                    var curInputId = prefix + key
                    newAttrs[key] = this.$el.find(curInputId).val()
                })

                this.model.set(newAttrs)
            })
        }

        onRender():void {
            //super.onRender();
        }

//it is not defined inside declaration because you could make it more dynamic if you wanted
        //template:(...params:any[]) => string; //old backbone stuff

        events():any {
            //return super.events();    //this is undefined, lets skip it
            return {
                "click button": (event:Event) => {
                    //console.log("button is poison")
                    this.model.destroy()
                }
            }
        }
    }
    applyMixins(EquipmentRequestItemView, [ViewTraits.BindingUiElements])
}
