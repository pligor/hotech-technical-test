/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/jquery.d.ts" />
/// <reference path="../../../declarations/underscore.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />
/// <reference path="../traits/ViewTraits.ts" />
/// <reference path="../typescript.ts" />
/// <reference path="../models/EventLocationModel.ts" />


/*this.on("remove", (model: EventLocationModel) => {
 model.destroy({
 success: (modelAfterSuccess, response) => {
 console.log("model " + model.id + " was deleted successfully")
 }
 })
 })*/

module Hotech {
    export class EventLocationItemView extends Marionette.ItemView<EventLocationModel> implements ViewTraits.BindingUiElements<EventLocationModel> {

        template = "script#eventvenue_template" //http://marionettejs.com/docs/v2.4.1/marionette.itemview.html

        //if you have it as attributes they are not working
        /*this.tagName = "tr"
         this.className = "noname"
         this.id = "malakas"*/
        //you need to set them as functions instead
        tagName() {
            return "div"
        }

        className() {
            return "event_venue"
        }

        ui() {
            //return super.ui();
            return {
                //currentYear: ".current_year"
            }
        }

        getUI:(arg:string) => JQuery;

        constructor(options?:Backbone.ViewOptions<EventLocationModel>) {
            super(options)

            //this.template = _.template($("#flowerElement").html())   //1) grab template

            //NO INTERACTION HAVE BEEN CREATED FOR THE MODEL YET, SO THIS IS UNNECESSARY
            /*this.model.on("change", () => {
             this.render()               //maybe we should remove this in the future, logo is not going to change
             })*/

            /*this.model.on("remove", (model: EventLocationModel) => {
                console.log("model removeddddddddddddddd")
                /!*model.destroy({
                    success: (modelAfterSuccess, response) => {
                        console.log("model " + model.id + " was deleted successfully")
                    }
                })*!/
            })*/
        }

        onInitialize(options:Backbone.ViewOptions<EventLocationModel>) {
            this.model.on("refreshYourself", () => {
                var prefix = "input#eventVenue_"

                //console.log(this)
                //console.log(this.model)

                var jsonObj = this.model.toJSON()
                delete jsonObj.id
                delete jsonObj.eventId

                var keys = Object.getOwnPropertyNames(jsonObj)

                //console.log(keys)

                var newAttrs = {}

                keys.forEach((key) => {
                    var curInputId = prefix + key
                    newAttrs[key] = this.$el.find(curInputId).val()
                })

                this.model.set(newAttrs)
            })
        }

        onRender():void {
            //super.onRender();
        }

//it is not defined inside declaration because you could make it more dynamic if you wanted
        //template:(...params:any[]) => string; //old backbone stuff

        events():any {
            //return super.events();    //this is undefined, lets skip it
            return {
                "click button": (event:Event) => {
                    //console.log("button is poison")
                    this.model.destroy()
                }
            }
        }

        /*addBgColor() {
         this.$el.css("background", "#94E4F1")
         }*/


        //defining render in marionette is optional
        //render():Marionette.ItemView<LogoModel> {
        //return super.render();
        //This is for decorating before rendering
        /*var jsonDeepCopy = JSON.parse(JSON.stringify(
         this.model.toJSON()
         ))
         jsonDeepCopy.price = jsonDeepCopy.price.toFixed(2)*/
        //in marionette this is handled automatically by default:
        //var flowerTemplate = this.template(jsonDeepCopy) //2) fill template in memory with data
        //in marionette this is handled automatically by default:
        //this.$el.html(flowerTemplate)   //3) load template in the element that we have defined (here is an <article>)
        //return this
        //}
    }
    applyMixins(EventLocationItemView, [ViewTraits.BindingUiElements])
}
