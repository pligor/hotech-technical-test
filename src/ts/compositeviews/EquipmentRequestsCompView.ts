/**
 * Created by pligor on 6/9/15.
 */

/// <reference path="../../../declarations/backbone.radio.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />
/// <reference path="../itemviews/EquipmentRequestItemView.ts" />
/// <reference path="../models/EquipmentRequestModel.ts" />

module Hotech {
    export class EquipmentRequestsCompView extends Marionette.CompositeView<EquipmentRequestModel> {
        template = "script#equipmentRequests_template"

        tagName():string {
            //return super.tagName();
            return "div"
        }

        className():string {
            //return super.className();
            return "equipment_requests"
        }

        constructor(options:Marionette.CollectionViewOptions<EquipmentRequestModel>) {
            super(options)

            this.childView = EquipmentRequestItemView

            this.childViewContainer = "div.the_requests"  //jquery selector
        }

        events():any {
            //return super.events();

            return {
                "click button.icon-plus": (event: Event) => {

                    var returnValue = this.collection.add(new EquipmentRequestModel())

                    //console.log(returnValue)
                }
            }
        }

        setEventIdToAllModels(eventId: string) {
            this.collection.forEach((model) => {
                model.trigger("thisIsYourParent", {
                    eventId: eventId
                })
            })
        }

        syncAllFormsWithModels() {
            this.collection.forEach((model) => {
                model.trigger("refreshYourself")
            })
        }

        syncAllModelsWithServer(finalSuccessCallback: () => void) {
            function recurse(curarr: EquipmentRequestModel[]) {
                var curModel = curarr.shift()   //take the first    //.pop() gets the last

                curModel.save({}, {
                    success: () => {

                        if(curarr.length == 0) {
                            finalSuccessCallback()

                            //end of recurse function
                        } else {
                            recurse(curarr)
                        }

                    }
                })
            }

            var array = this.collection.toArray()

            if(array.length == 0) {
                finalSuccessCallback()
            } else {
                recurse(array)
            }
        }
    }
}
