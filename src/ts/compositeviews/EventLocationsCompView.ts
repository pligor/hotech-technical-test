/**
 * Created by pligor on 6/9/15.
 */

/// <reference path="../../../declarations/backbone.radio.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />
/// <reference path="../itemviews/EventLocationItemView.ts" />
/// <reference path="../models/EventLocationModel.ts" />

module Hotech {
    export class EventLocationsCompView extends Marionette.CompositeView<EventLocationModel> {
        template = "script#eventLocations_template"

        tagName():string {
            //return super.tagName();
            return "div"
        }

        className():string {
            //return super.className();
            return "event_locations"
        }

        constructor(options:Marionette.CollectionViewOptions<EventLocationModel>) {
            super(options)

            this.childView = EventLocationItemView

            this.childViewContainer = "div.venues"  //jquery selector
        }

        events():any {
            //return super.events();

            return {
                "click button.icon-plus": (event: Event) => {

                    var returnValue = this.collection.add(new EventLocationModel())

                    //console.log(returnValue)
                },
                /*"click a.icon-trash-empty": (event: Event) => {
                    var curId = getIdFromEvent(event)

                    //console.log("delete: " + curId)

                    this.collection.remove(curId)   //Fires a "remove" event for each model, and a single "update" event afterwards
                },
                "click button": (event: Event) => {
                    window.location.href = "form.html"
                }*/
            }
        }

        setEventIdToAllModels(eventId: string) {
            this.collection.forEach((model) => {
                model.trigger("thisIsYourParent", {
                    eventId: eventId
                })
            })
        }

        syncAllFormsWithModels() {
            this.collection.forEach((model) => {
                model.trigger("refreshYourself")
            })
        }

        syncAllModelsWithServer(finalSuccessCallback: () => void) {
            function recurse(curarr: EventLocationModel[]) {
                var curModel = curarr.shift()   //take the first    //.pop() gives the last

                //console.log(curModel)

                curModel.save({}, {
                    success: () => {

                        if(curarr.length == 0) {
                            finalSuccessCallback()

                            //end of recurse function
                        } else {
                            recurse(curarr)
                        }

                    }
                })
            }

            var array = this.collection.toArray()

            if(array.length == 0) {
                finalSuccessCallback()
            } else {
                recurse(array)
            }
        }
    }
}
