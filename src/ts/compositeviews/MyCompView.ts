/**
 * Created by pligor on 6/9/15.
 */

/// <reference path="../../../declarations/backbone.radio.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />
/// <reference path="../itemviews/RowItemView.ts" />
/// <reference path="../models/EventModel.ts" />

module Hotech {
    export class MyCompView extends Marionette.CompositeView<EventModel> {
        template = "script#event_template"

        /*itemView() {
            console.log ("item view called")

            return RowItemView
        }*/

        /*itemViewContainer() {
            console.log("item view container")

            return "tbody" //jquery selector
        }*/

        //itemView = RowItemView
        //itemViewContainer = "tbody" //jquery selector

        attachToTheDOM() {
            $("body").prepend(this.render().el)
        }

        constructor(options:Marionette.CollectionViewOptions<EventModel>) {

            super(options)

            this.childView = RowItemView

            this.childViewContainer = "tbody"
        }

        events():any {
            //return super.events();

            function getIdFromEvent(event: Event) {
                return $(event.target).data("eventid")
            }

            return {
                "click a.icon-pencil": (event: Event) => {
                    //console.log("edit: " + getIdFromEvent(event))

                    sessionStorage.setItem("event_id", getIdFromEvent(event))

                    window.location.href = "form.html"
                },
                "click a.icon-trash-empty": (event: Event) => {
                    var curId = getIdFromEvent(event)

                    //console.log("delete: " + curId)

                    this.collection.remove(curId)   //Fires a "remove" event for each model, and a single "update" event afterwards
                },
                "click button": (event: Event) => {
                    sessionStorage.removeItem("event_id")

                    window.location.href = "form.html"
                }
            }
        }
    }
}
