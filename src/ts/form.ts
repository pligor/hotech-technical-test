/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../declarations/marionette.d.ts" />
/// <reference path="./typescript.ts" />
/// <reference path="./layouts/MainLayout.ts" />

class FormMarionetteApp extends Marionette.Application {
    //flowerStaticDataModule:FlowerStaticDataModule = null;

    constructor(/*modules: any[]*/) {
        super()
        //this.someModule = someModule;
    }

    topview:Hotech.MainLayout = null

    //theRouter: Marionette.AppRouter
}

var App = new FormMarionetteApp(/*{
 channelName: 'pligorAppChannel' //can be configured like that, but channel name = 'global' is also fine
 }*/)

//all initializers called with App.start()
/**
 * deprecated
 */
/*App.addInitializer(function (options:any) {
 //...
 })*/

//these are not triggered, I do not know why
/*App.on("initialize:before", function (options:any) {
 options.anotherThing = true
 dis("initialization starting")
 })

 App.on("initialize:after", function (options:any) {
 dis("initialization finished")
 })

 App.on("initialize", function (options:any) {
 dis("initialization in general")
 })*/

App.on("start", (options:any) => {


    //var coll = new Backbone.Collection({})

    App.topview = new Hotech.MainLayout(/*{model: App.mainModel}*/)

    App.topview.attachToTheDOM()
})

App.start({
    //pass the options here
})
