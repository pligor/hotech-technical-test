/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/backbone.d.ts" />
/// <reference path="../typescript.ts" />

//import backbone = require("backbone") //we are NOT using external modules for now to avoid require.js
//declare var $: any; //in order for the file to compile successfully unless you include the declaration of jquery

/*provideLogoDataStatically() {
 //App.layoutView.getRegion("headerRegion")  //for dynamic situations
 //var headerLayout:Pligor.HeaderLayout = App.layoutView.headerRegion.currentView    //we are already in here
 var headerLayout:Pligor.HeaderLayout = this.headerRegion.currentView;
 var logoItemView:Pligor.LogoItemView = headerLayout.logoRegion.currentView
 var curModel = logoItemView.model;
 /!*curModel.fetch({
 success: function (model, response, options) {
 console.log("log data fetched ok")
 }
 })*!/
 curModel.set({
 logoImage: "img/pligoropoulos_logo.png",
 logoTitle: "pligoropoulos",
 logoSubtitle: "ενεργειακες λυσεις"
 })
 }*/

module Hotech {
    export class EventLocationModel extends Backbone.Model {
        url() {
            if(this.id === undefined) {
                return this.urlRoot
            } else {
                return this.urlRoot + "/" + this.id + "?eventId=" + this.get("eventId")
            }
        }

        constructor(attributes?:any, options?:any) {
            super(attributes, options);

            this.urlRoot = "http://localhost:3000/locations"

            this.on("thisIsYourParent", (payload) => {
                this.set("eventId", payload.eventId)
            })
        }

        defaults():any {
            //return super.defaults();
            return {
                venueName: "write venue name here",
                city: "write city here",
                state: "write state here",
                country: "write country here",
                eventId: null
            }
        }

        initialize(attributes:any, options:any):void {
            super.initialize(attributes, options)  //empty function by default in models

            /*if (attributes === undefined) {
             attributes = {}
             } else {
             //nop
             }*/

            /*if (this.id === undefined) {
                //console.log("create new model id")

                //this.set("id", guid())    //NO NEED the exact same thing is returned by the json-server when model is saved as new
            } else {
                //this.fetch()
            }*/
        }
    }

    /*export interface SingleFlowerInitAttributes {
     name: string
     price: number
     color?: string
     img?: string
     link: string
     }*/
}
