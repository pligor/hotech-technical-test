/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/backbone.d.ts" />
/// <reference path="../typescript.ts" />

//import backbone = require("backbone") //we are NOT using external modules for now to avoid require.js
//declare var $: any; //in order for the file to compile successfully unless you include the declaration of jquery

/*provideLogoDataStatically() {
 //App.layoutView.getRegion("headerRegion")  //for dynamic situations
 //var headerLayout:Pligor.HeaderLayout = App.layoutView.headerRegion.currentView    //we are already in here
 var headerLayout:Pligor.HeaderLayout = this.headerRegion.currentView;
 var logoItemView:Pligor.LogoItemView = headerLayout.logoRegion.currentView
 var curModel = logoItemView.model;
 /!*curModel.fetch({
 success: function (model, response, options) {
 console.log("log data fetched ok")
 }
 })*!/
 curModel.set({
 logoImage: "img/pligoropoulos_logo.png",
 logoTitle: "pligoropoulos",
 logoSubtitle: "ενεργειακες λυσεις"
 })
 }*/

module Hotech {
    export class EventModel extends Backbone.Model {

        constructor(attributes?:any, options?:any) {
            super(attributes, options);

            this.urlRoot = "http://localhost:3000/events"; //this.url will be .../<the model id>
        }

        defaults():any {
            //return super.defaults();
            return {
                eventName: "Test Event",
                businessGroup: "HP Enterprise Services",
                eventType: "HP event",
                eventRegion: "EMEA",
                timezone: "-05:00",
                customerSegment: "SOHO",
                startDate: "2015-06-08",
                endDate: "2015-06-12",
                mrmMasterCampaignName: "HR Campaign",
                firstname: "teo",
                lastname: "",
                email: "tcg@hotech.gr",
                officePhone: "",
                mobilePhone: ""
            }
        }

        initialize(attributes:any, options:any):void {
            super.initialize(attributes, options);  //empty function by default in models

            //console.log("A model instance named " + this.get("name") + "has been created and it costs " + this.get("price"))

            /*this.on("change:price", function () {    //:price makes it specific to a certain attribute
             console.log(
             "The price for the " + this.get("name") + " model just changed to $" + this.get("price") + " dollars"
             )
             })*/

            /*if (attributes === undefined) {
                attributes = {}
            } else {
                //nop
            }*/

            /*if (this.id === undefined) {
                //console.log("create new model id")

                //this.set("id", guid())    //NO NEED the exact same thing is returned by the json-server when model is saved as new
            } else {
                //this.fetch()
            }*/
        }
    }

    /*export interface SingleFlowerInitAttributes {
     name: string
     price: number
     color?: string
     img?: string
     link: string
     }*/
}
