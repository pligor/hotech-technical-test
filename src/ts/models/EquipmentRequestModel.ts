/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/backbone.d.ts" />
/// <reference path="../typescript.ts" />

//import backbone = require("backbone") //we are NOT using external modules for now to avoid require.js
//declare var $: any; //in order for the file to compile successfully unless you include the declaration of jquery

module Hotech {
    export class EquipmentRequestModel extends Backbone.Model {
        url() {
            if(this.id === undefined) { //POST situation
                return this.urlRoot
            } else {    //PUT, GET and DELETE
                return this.urlRoot + "/" + this.id + "?eventId=" + this.get("eventId")
            }
        }

        constructor(attributes?:any, options?:any) {
            super(attributes, options);

            this.urlRoot = "http://localhost:3000/eqrequests"

            this.on("thisIsYourParent", (payload) => {
                this.set("eventId", payload.eventId)
            })
        }

        defaults():any {
            //return super.defaults();
            return {
                deviceType: "write device type here",
                qtyRequired: "0",
                qtyToShip: "0",
                comments: "write any comments in here",
                eventId: null
            }
        }

        initialize(attributes:any, options:any):void {
            super.initialize(attributes, options)  //empty function by default in models
        }
    }

    /*export interface SingleFlowerInitAttributes {
     name: string
     price: number
     color?: string
     img?: string
     link: string
     }*/
}
