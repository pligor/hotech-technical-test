/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/backbone.d.ts" />
/// <reference path="../models/EquipmentRequestModel.ts" />

//merging modules by using same name
module Hotech {
    export class EquipmentRequestsCollection extends Backbone.Collection<EquipmentRequestModel> {
        //I make the models as required (bootstrapping suggested by backbone) but I will leave them as they are

        parentId: string = null

        url() {
            return "http://localhost:3000/eqrequests" + (this.parentId === null ? "" : "?eventId=" + this.parentId)
        }

        constructor(models: EquipmentRequestModel[], options?: any) {
            super(models, options)

            this.model = EquipmentRequestModel

            if(options && options.parentId) {
                this.parentId = options.parentId

                this.on("add", (model: EquipmentRequestModel) => {
                    //console.log("adding event loc")

                    //insert the id into the model if it does not exist
                    if(model.get("eventId") === null) {
                        model.set("eventId", this.parentId)
                    } else {
                        //none
                    }

                    //console.log(model.get("eventId"))
                })
            } else {

            }
        }
    }
}
