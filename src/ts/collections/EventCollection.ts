/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../../declarations/backbone.d.ts" />
/// <reference path="../models/EventModel.ts" />

//merging modules by using same name
module Hotech {
    export class EventCollection extends Backbone.Collection<EventModel> {

        /*url() {
            return "http://localhost:3000/events"
        }*/

        constructor(models: EventModel[], options?: any) {
            super(models, options)

            this.model = EventModel

            this.url = "http://localhost:3000/events"

            this.on("remove", (model: EventModel) => {
                model.destroy({
                    success: (modelAfterSuccess, response) => {
                        console.log("model " + model.id + " was deleted successfully")
                    }
                })
            })
        }
    }
}
