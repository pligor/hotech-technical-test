/**
 * Created by pligor on 4/20/15.
 */
/// <reference path="../../../declarations/backbone.radio.d.ts" />
/// <reference path="../../../declarations/marionette.d.ts" />

/// <reference path="../collections/EquipmentRequestsCollection.ts" />
/// <reference path="../collections/EventLocationsCollection.ts" />
/// <reference path="../compositeviews/EventLocationsCompView.ts" />
/// <reference path="../compositeviews/EquipmentRequestsCompView.ts" />
/// <reference path="../itemviews/EventSetupItemView.ts" />
/// <reference path="../radio/RadioEvents.ts" />

module Hotech {

    /*interface MyRegion {
     regionName: string
     regionSelector: string
     }

     interface MyRegionJS {
     [index: string]: string
     }

     class MyRegionsCollection {
     regions:MyRegion[];

     addRegion(jsObj:MyRegionJS) {

     }

     getJsObj() {
     var jsObj:any;

     this.regions.forEach((curRegion, index/!*, array*!/) => {
     jsObj[curRegion.regionName] = curRegion.regionSelector
     })

     return jsObj
     }
     }*/

    /**
     * We cover everything in this file from here:
     * http://marionettejs.com/docs/v2.4.1/marionette.layoutview.html#specifying-regions-as-a-function
     */
    export class FormContentLayout extends Marionette.LayoutView<Backbone.Model> {

        template = "script#FormContentLayout_template"

        //destroyImmediate = false // This option removes the layoutView from the DOM before destroying the children
        // preventing repaints as each option is removed. However, it makes it difficult to do close animations
        // for a child view (false by default)

        /*typedRegions(options) {
         var regs = new MyRegionsCollection

         regs.regions.push({header: "#header"})
         }*/

        tagName():string {
            //return super.tagName();
            return "div"
        }

        className():string {
            //return super.className();
            return "form_content_layout"
        }

        /**
         * Any region can be removed, whether it was defined in the regions attribute of the region definition, or added later.
         * @param options
         */
        regions(options:any) {
            return {
                /*  //sample of how you can have a custom region class for each region!
                 menu: {
                 selector: "#menu",
                 regionClass: CustomRegionClassReference
                 }*/
                eventsetupRegion: "article#eventsetup",  //#header is unnecessary since it works like a jquery selector
                //(not exactly the same though because body > header is not working)
                eventlocationsRegion: "article#eventlocations",
                equipmentrequestsRegion: "article#equipmentrequests",
            }
        }

        eventsetupRegion:Marionette.Region //to have it statically typed
        eventlocationsRegion:Marionette.Region
        equipmentrequestsRegion:Marionette.Region

        //in order to change the model of the main layout due to a child event (any depth child) then we need to propagate

        childEvents() { //this can also be a variable if you want something more static
            return {
                /*render: (childView) => { //this callback will be called whenever a child is rendered or emits a "render" event
                 console.log("a childview has been rendered")
                 },*/
            }
        }

        constructor(options?:any) {  //we make it optional here
            super(options);
            //Avoid Re-Rendering The Entire LayoutView
            /*Instead, if you are binding the
             layoutView's template to a model and need to update portions of the layoutView,
             you should listen to the model's "change" events and only update the
             necessary DOM elements.
             WHICH MEANS WE SHOULD DO jQUERY*/
            /*this.model.on("change:breadcrumb", (curModel:Pligor.MainModel) => {
             this.$el.find(".breadcrumb_class").html(curModel.get("breadcrumb"))
             })*/
        }

        initialize(options:Backbone.ViewOptions<Backbone.Model>):void {
            super.initialize(options);
        }

        events():any {
            //return super.events();
            return {
                "click .save_button > button": () => {
                    //console.log("save here")

                    var curEventSetupItemView:EventSetupItemView = this.eventsetupRegion.currentView

                    curEventSetupItemView.collectValuesAndSaveModel((model:EventModel) => {

                        var curEventLocationsCompView:EventLocationsCompView = this.eventlocationsRegion.currentView

                        curEventLocationsCompView.setEventIdToAllModels(model.id)
                        curEventLocationsCompView.syncAllFormsWithModels()
                        curEventLocationsCompView.syncAllModelsWithServer(() => {

                            var curEquipmentRequestsCompView:EquipmentRequestsCompView = this.equipmentrequestsRegion.currentView

                            curEquipmentRequestsCompView.setEventIdToAllModels(model.id)

                            curEquipmentRequestsCompView.syncAllFormsWithModels()
                            curEquipmentRequestsCompView.syncAllModelsWithServer(() => {
                                //console.log("synced with the server successfully")

                                sessionStorage.removeItem("event_id")
                                //console.log("model is saved in server with id: " + model.id)

                                window.location.href = "table.html"
                            })
                        })
                    })
                }
            }
        }

        /**
         * As a rule of thumb, most of the time you'll want to render any nested views in the onBeforeRender callback.
         */
        onBeforeRender():void {
            //super.onBeforeRender();
        }

        onRender():void {
            //super.onRender();

            //rendering the children

            //this.showChildView('footer', new FooterView());   //footer is the region name

            var eventId = sessionStorage.getItem("event_id")

            var curModel = eventId === null ? new EventModel() : new EventModel({id: eventId})
            //console.log(curModel.id)
            //console.log(curModel.get("id"))
            //return;

            var renderEventLocations = (curCollection:EventLocationsCollection) => {
                this.eventlocationsRegion.show(new EventLocationsCompView({collection: curCollection}))
            }

            var renderEquipmentRequests = (curCollection:EquipmentRequestsCollection) => {
                this.equipmentrequestsRegion.show(new EquipmentRequestsCompView({collection: curCollection}))
            }

            var renderModel = (model:EventModel) => {
                this.eventsetupRegion.show(new EventSetupItemView({model: model}))

                if (model.isNew()) {
                    renderEventLocations(new EventLocationsCollection(
                        []
                    ))

                    renderEquipmentRequests(new EquipmentRequestsCollection(
                        []
                    ))
                } else {
                    new EventLocationsCollection([], {
                        parentId: model.id
                    }).fetch(
                        {
                            success: (curColl:EventLocationsCollection) => {
                                renderEventLocations(curColl)
                            }
                        }
                    )

                    new EquipmentRequestsCollection([], {
                        parentId: model.id
                    }).fetch(
                        {
                            success: (curColl:EquipmentRequestsCollection) => {
                                renderEquipmentRequests(curColl)
                            }
                        }
                    )
                }
            }

            if (eventId === null) {
                renderModel(curModel)
            } else {
                curModel.fetch({
                    success: (retrievedModel) => {
                        renderModel(retrievedModel)
                    }
                })
            }

            //this.eventlocationsRegion.show(new FormContentLayout());
            //this.equipmentrequestsRegion.show(new FormContentLayout());

            /*this.listenTo(this.announcementRegion.currentView, "poisonpill", () => {
             this.announcementRegion.empty() //remove the region contents
             });*/
        }
    }
}
