/**
 * Created by pligor on 4/17/15.
 */

/// <reference path="../../declarations/marionette.d.ts" />
/// <reference path="./typescript.ts" />
/// <reference path="./compositeviews/MyCompView.ts" />
/// <reference path="./collections/EventCollection.ts" />

class TableMarionetteApp extends Marionette.Application {
    //flowerStaticDataModule:FlowerStaticDataModule = null;

    constructor(/*modules: any[]*/) {
        super()
        //this.someModule = someModule;
    }

    topview:Hotech.MyCompView = null

    //theRouter: Marionette.AppRouter
}

var App = new TableMarionetteApp(/*{
 channelName: 'pligorAppChannel' //can be configured like that, but channel name = 'global' is also fine
 }*/)

//all initializers called with App.start()
/**
 * deprecated
 */
/*App.addInitializer(function (options:any) {
 //...
 })*/

//these are not triggered, I do not know why
/*App.on("initialize:before", function (options:any) {
 options.anotherThing = true
 dis("initialization starting")
 })

 App.on("initialize:after", function (options:any) {
 dis("initialization finished")
 })

 App.on("initialize", function (options:any) {
 dis("initialization in general")
 })*/

App.on("start", (options:any) => {
    /*{model: App.mainModel}*/

    //var coll = new Backbone.Collection({})

    App.topview = new Hotech.MyCompView({collection: new Hotech.EventCollection([
        //empty
    ])})

    App.topview.collection.fetch({
        success: function (collection, response, options) {
            console.log(collection.length)
        }
    })

    App.topview.attachToTheDOM()
})

App.start({
    //pass the options here
})
